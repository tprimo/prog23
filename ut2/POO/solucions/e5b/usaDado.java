public class usaDado
{

	public static void main (String args[])
	{
		int tiradas;
		dado d1= new dado();	// constructor per defecte
		dado d2= new dado(5);	// constructor general
		dado d3= new dado(d2);	// constructor de còpia
		
		d1.muestraDado();
		d2.muestraDado();
		d3.muestraDado();
		
		//tirada2();
		tiradas = d2.genera666();
		System.out.println("Han fet falta " + tiradas + " per a generar 3 sisos consecutius");
	}

}
