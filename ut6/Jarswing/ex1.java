import javax.swing.*;

public class ex1
{
	public static void main(String[] args) 
	{
		JFrame frame = new JFrame("Hello World");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// Es CREA L'ETIQUETA, INICIALMENT INDEPENDIENT DE LA finestra
		JLabel label= new JLabel("Hola DAM1");
		// S'AFEGEIX A LA FINESTRA
		frame.getContentPane().add(label);
		// ESTABLEIX LA MIDA DE LA FINESTRA PER DEFECTE AL MÍNIM DELS SEUS COMPONENTS
		frame.pack();
		// CENTREM LA FINESTRA AMB EL VALOR NULL
		frame.setLocationRelativeTo(null);
		// LA FEM VISIBLE
		frame.setVisible(true);
	}
}
