/*
   2. Crear un objecte ResultSet sobre la taula clients que permeta desplaçament lliure, però que no veja els canvis en les dades i permeta realitzar modificacions sobre les dades.
**/
import java.sql.*;

public class ex2{
  
        public static void main (String args[]){
   
            try(
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa","root","");
                Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = stmt.executeQuery("select * from clientes");
            ){
                int cod;
                String nombre;
                String direccion;

                while(rs.next()){
                    cod=rs.getInt(1);                   
                    if(cod==1){
                        rs.updateString(3,"Liberty Avenue 373");
                        rs.updateRow();
                    }  
                    nombre=rs.getString(2);
                    direccion=rs.getString(3);
                    System.out.println("ID: "+cod+"\t"+"Nom: "+nombre+"\t"+"Adreça: "+direccion);
                }
            }
            catch(SQLException se){
                System.err.println(se.getMessage());
            }

        }

}
