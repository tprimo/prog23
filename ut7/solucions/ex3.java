/*
3. Es vol realitzar un programa en Java que, de manera atòmica, 
realitze un increment d'un 10% en el sou de tots els seus venedors 
i faça un increment d'un 5% en el preu de tots els seus articles de la base de dades empresa.
*/
import java.sql.*;


public class ex3 {

    public static void main(String[] args) {

        try (Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/empresa", "root", "root")) {
            try (Statement stmt = conn.createStatement()) {
                // INICI DE LA TRANSACCIÓ
                conn.setAutoCommit(false);
                String sql = "update vendedores set salario = salario + salario*0.1";
                stmt.executeUpdate(sql);
                // Si el id no existeix, llançarà l'excepció: anem a forçar que la llanci
                sql = "update articulos set precio = precio + precio*0.05";
                stmt.executeUpdate(sql);

                // Finalització de la transacció
                conn.commit();
                conn.setAutoCommit(true);
            } catch (SQLException se) {
                conn.rollback();
                conn.setAutoCommit(true);
                throw se;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
