/* Ejemplo de SELECT utilizando RESULTSET y posterior DELETE
 */
import java.sql.*;

public class ex13ResultSetInsertRow {
    
    public static void main(String[] args) {
        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/musica","root","root");
        	//Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
        	Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                      ResultSet.CONCUR_UPDATABLE);
        	ResultSet rs = stmt.executeQuery("select * from discos");	
        ) {
            int cod,nMusic; String titol; double preu; 
            while (rs.next())
            {
            	cod = rs.getInt(1);
		if (cod ==6)
		{
			rs.moveToInsertRow();
			rs.updateInt("id",5);
			rs.updateString("titol","Street Legal");
			rs.updateDouble("preu",16.5);
			rs.updateInt("music",1);
			rs.insertRow();
			rs.moveToCurrentRow();
		}
            	titol = rs.getString(2);
            	preu = rs.getDouble("preu");
            	nMusic = rs.getInt("music");
            	System.out.println("Id: " + cod + ",\t" + titol + ", preu: " + preu + " euros, del music " + nMusic);
            }
            
        } catch(SQLException se) {
            //Errors de JDBC
            se.printStackTrace();
        }
    }
}
