/* Fer un exercisi que demane al usuari un nombre de segons i et mostre aquest valor amb hores,minuts i segons.*/

public class segons_HMS
{
    public static void main (String args[])
    {
        int seg, hores, min;

        System.out.println("Introdueix un valor de segons per a fer la conversió: ");
        seg = Integer.parseInt(System.console().readLine());
        
        hores = seg / 3600;
        seg = seg % 3600;
        min = seg / 60;
        seg = seg % 60;

        System.out.println("Son " + hores + " hores," + min + " minuts i " + seg + " segons.");
    }
}
