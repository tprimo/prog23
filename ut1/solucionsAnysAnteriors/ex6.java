/*6. Programa que accepta 2 nombres i, si la seva suma és inferior a 10, demana un tercer valor. A la fi, mostra la suma dels valors introduïts.*/

public class ex6
{
    public static void main(String args[])
    {
        double num1, num2, num3;
        System.out.println("Introdueix dos valors: ");
        num1 = Double.parseDouble(System.console().readLine());
        num2 = Double.parseDouble(System.console().readLine());

        if(num1 + num2 < 10)
        {
            System.out.println("Introdueix altre valor: ");
            num3 = Double.parseDouble(System.console().readLine());
            System.out.println("El resultat es " + (num1 + num2 + num3) + " .");
        }
        else
            System.out.println("El resultat es " + (num1 + num2) + " .");
/*        if(num1 + num2 < 10)
        {
            System.out.println("Introdueix altre valor: ");
            num3 = Double.parseDouble(System.console().readLine());
            //System.out.println("El resultat es " + (num1 + num2 + num3) + " .");
        }
        else
        	num3=0;
        // fora de la alternativa
        System.out.println("El resultat es " + (num1 + num2 + num3) + " .");*/
    }
}

