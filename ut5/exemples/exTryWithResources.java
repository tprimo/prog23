/* Programa que escriga 100 números de loteria, aleatoris , a un fitxer binari.

	EXEMPLE DE TRY-WITH-RESOURCES */

import java.io.*;

class exTryWithResources
{
	public static void main(String[] args) {
			final int LIM = 100;
			final int TAM = 100000;
			try(FileOutputStream fos = new FileOutputStream("nums2.dat");
				DataOutputStream dos = new DataOutputStream(fos); )
			{
				//FileOutputStream fos = new FileOutputStream("nums.dat");
				//DataOutputStream dos = new DataOutputStream(fos);
				for (int i=0 ; i < LIM ; i++)
				{
					int num = (int) (TAM*Math.random());
					System.out.println(num);
					// escric a fitxer el número
					dos.writeInt(num);
				}
				// NO HE DE TANCLAR DOS I FOS PERQUÈ SÓN AUTOCLOSEABLES
				//dos.close();
				//fos.close();
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
	}
}
