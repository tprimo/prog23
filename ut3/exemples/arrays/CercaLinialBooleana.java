// exemple de cerca d'un valors dins d'un array

import java.util.Scanner;

public class CercaLinialBooleana
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		final int N = 5; double valor; boolean trobat;
		
		// creem l'array de 4 elements
		double nums[] = new double[N];
		// carregar valors des de teclat
		carregaValors(nums);
		// mostrar valors de l'array en pantalla
		mostraValors(nums);
		// demanaré un valor a cercar
		System.out.println("Introduix un valor de recerca:");	
		valor = ent.nextDouble();
		trobat = cerca(nums,valor);
		if (trobat)
			System.out.println("El valor SI s'ha trobat a l'array");
		else	
			System.out.println("El valor NO s'ha trobat a l'array");
		
	}
	
	public static void carregaValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
		{
			System.out.println("Introduix un valor numèric:");
			nums[i] = ent.nextDouble();
		}
	}
	
	public static void mostraValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
		System.out.println("");
	}
	
	public static boolean cerca(double nums[], double v)
	{
		for (int i=0 ; i < nums.length ; i++)
			if (nums[i] == v)
				return true;
		return false;
	}
}
