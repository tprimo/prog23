// Primer exemple d'array de tipus DOUBLE (tipus bàsic)
import java.util.Scanner;

public class arrayDouble
{
	public static void main(String args[])
	{
		final int N = 4;
		Scanner ent = new Scanner(System.in);
		
		// creem l'array de 4 elements
		double nums[] = new double[N];
		// carregar valors des de teclat
		for (int i=0 ; i < N ; i++)
		{
			System.out.println("Introduix un valor numèric:");
			nums[i] = ent.nextDouble();
		}
		// mostrar valors de l'array en pantalla
		for (int i=0 ; i < N ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
			
			
		
	}
}
