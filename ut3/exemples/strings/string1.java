public class string1
{
	public static void main(String args[])
	{
		//String s = new String("sergio");
		String s = "Sergio";
		String s2 = s;
		
		s = "Daniel";
		System.out.println(s);
		System.out.println(s2);
		
		for(int i=0 ; i < s.length() ; i++)
			System.out.println(s.charAt(i));
			
		// El nom té x caracters de longitud
		System.out.println("El nom té una longitud de " + s.length() + " caracters");
		
		if (s.isEmpty())
			System.out.println("La cadena està buïda");
		else
		
			System.out.println("La cadena té contingut");
		s = "";	
		if (s.isEmpty())
			System.out.println("La cadena està buïda");
		else
		
			System.out.println("La cadena té contingut");
		s ="    ";	
		if (s.isBlank())
			System.out.println("La cadena només té espais en blanc");
		else
		
			System.out.println("La cadena té contingut");
		
		s = "Salamanca";
		// Canviaré cada aparició del caracter 'a' per 'o'
		s = s.replace('a','o');
		System.out.println(s);
		s = s.replace("Sol","Salpimentar");
		System.out.println(s);
		
		// Anem a comparar lexicogràficament els 2 strings
		if (s.compareTo(s2) < 0)
			System.out.println(s + " es inferior a " + s2);
		else
			System.out.println(s2 + " es inferior a " + s);
		System.out.println(s.compareTo(s2));
		
		// també puc reemplaçar caracters treballant amb el text com a array
		s = "Salamanca";
		char array[] = s.toCharArray();	// de String a char[]
		// ara tinc l'array següent: {'S','a','l','a,'m','a','n','c','a'}
		for (int i= 0; i < array.length ;i++)
			if (array[i] == 'a')
				array[i] = 'o';
		// de char[] a String
		s = String.valueOf(array);
		System.out.println(s);
		
		
	}
}
