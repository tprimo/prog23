// 
import java.util.Scanner;

class array
{
	// l'array és atribut de la classe
	private int nums[];
	final int N=100;
	
	// constructor
	public array(){
		nums = new int[N];
	}

	// cercaLinialCont
	public int cercaLinialCont(int v)	// només quan l'array estiga ordenat
	{
		int cont=0;
		for (int i=0; i < nums.length ; i++)
			if (nums[i] > v)
				return cont;
			else if (nums[i] == v)
				cont++;
		return cont;
	}

	// cercaLineal
	public boolean cercaLineal(int v)
	{
		for (int i=0; i < nums.length ; i++)
			if (nums[i] == v)
				return true;
		return false;
	}

	// ordenació
	public void ordena()
	{
		int limit = nums.length - 2, i;
		int aux; boolean ordenat=false;
		
		while ((limit >= 0) && (ordenat == false))
		{
			ordenat = true;
			i = 0;
			while (i <= limit)
			{
				if (nums[i+1] < nums[i])
				{
					ordenat = false;
					aux = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = aux;
				}
				i++;
			}
			limit--;
		}
				
	}
	
	//cerca dicotomica
	public boolean cercaDicotomica(int valor)
	{
		int esq=0,dr=nums.length-1,centre;
		
		centre = (esq+dr)/2;
		while ((esq <= dr) && (nums[centre] != valor))
		{
			if (nums[centre] > valor)
				dr = centre - 1;
			else
				esq = centre + 1;
			centre = (esq+dr)/2;
		}
		if (nums[centre] == valor)
			return true;
		return false;
	}
	
	// mètode que carregue valors
	public void carregaValors()
	{
		for (int i=0; i < nums.length; i++)
			nums[i] = (int)(N*Math.random()) +1;
	}
	
	public void mostraValors()
	{
		for (int i=0 ; i < nums.length ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
		System.out.println("");
	}
}

public class e4
{
	public static void main(String args[])
	{
		int valor;
		Scanner ent = new Scanner(System.in);
		array a1 = new array();
		
		a1.carregaValors();
		a1.mostraValors();

		System.out.println("Introduce un valor entre 1 y 100 para buscarlo en la array: ");
		valor = ent.nextInt();

		if (a1.cercaLineal(valor))
			System.out.println("El valor se ha encontrado.");
		else
			System.out.println("El valor no se ha encontrado.");

		a1.ordena();
		a1.mostraValors();

		System.out.println("Introduce otro valor entre 1 y 100 para buscarlo en la array: ");
		valor = ent.nextInt();

		if (a1.cercaDicotomica(valor))
			System.out.println("El valor se ha encontrado.");
		else
			System.out.println("El valor no se ha encontrado.");

		System.out.println("Número de veces encontrado: " + a1.cercaLinialCont(valor));
		
	}
}
